package com.example.workflow.delegate;

import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class ParamApiDelegate implements JavaDelegate {


    @Value("${services.baseUrl}")
    private String baseUrl;

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {
        execute(delegateExecution, "", "GET");
    }

    public void execute(DelegateExecution delegateExecution, String path, String method) throws Exception {
        try {
            RequestEntity<Void> entity = new RequestEntity<>(HttpMethod.valueOf(method.toString()), URI.create(baseUrl + path));
            restTemplate.exchange(entity, Void.class);
        } catch (HttpClientErrorException e) {
            if (e.getRawStatusCode() == 400) {
                throw new BpmnError("BUSINESS_ERROR");
            } else if (e.getRawStatusCode() == 500) {
                throw new RuntimeException("TECHNICAL_ERROR");
            }
        }
    }
}
