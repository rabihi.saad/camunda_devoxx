import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests;
import org.camunda.bpm.engine.test.junit5.ProcessEngineExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

@ExtendWith(MockitoExtension.class)
@org.camunda.bpm.engine.test.Deployment(resources = {
        "cardinalityParalleleMultiInstanceUserTask.bpmn",
        "cardinalitySequenceMultiInstanceUserTask.bpmn",
        "collectionParalleleMultiInstanceUserTask.bpmn",
        "collectionSequenceMultiInstanceUserTask.bpmn"})
public class MultiInstanceSpringComponent {

    @RegisterExtension
    ProcessEngineExtension extension = ProcessEngineExtension.builder()
            .build();


    @Test
    public void cardinalityParalleleMultiInstanceUserTask() {
        Map<String, Object> vars = new HashMap<>();
        vars.put("nbrOfTasks", 4);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("cardinalityParalleleMultiInstanceUserTask", vars);

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        List<Task> tasks = taskService().createTaskQuery().list();

        int loopCounter = 1;

        for (Task task : tasks) {
            Assertions.assertEquals("Say hello " + loopCounter++ + "/4", task.getName());
            taskService().complete(task.getId());
        }

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "EndEvent"});
    }

    @Test
    public void cardinalitySequenceMultiInstanceUserTask() {
        Map<String, Object> vars = new HashMap<>();
        vars.put("nbrOfTasks", 4);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("cardinalitySequenceMultiInstanceUserTask", vars);

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        for (int loopCounter = 0; loopCounter < 4; loopCounter++) {
            Task task = taskService().createTaskQuery().singleResult();
            Assertions.assertEquals("Say hello " + (loopCounter + 1) + "/4", task.getName());
            taskService().complete(task.getId());
        }

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "EndEvent"});
    }
    @Test
    public void collectionParalleleMultiInstanceUserTask() {
        List<String> names = Arrays.asList("Saad", "Ahmed");
        Map<String, Object> vars = new HashMap<>();
        vars.put("names", names);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("collectionParalleleMultiInstanceUserTask", vars);

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        List<Task> tasks = taskService().createTaskQuery().list();

        int loopCounter = 0;

        for (Task task : tasks) {
            Assertions.assertEquals("Say hello " + names.get(loopCounter++), task.getName());
            taskService().complete(task.getId());
        }

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "EndEvent"});
    }

    @Test
    public void collectionSequenceMultiInstanceUserTask() {
        List<String> names = Arrays.asList("Saad", "Ahmed");
        Map<String, Object> vars = new HashMap<>();
        vars.put("names", names);
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("collectionSequenceMultiInstanceUserTask", vars);

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        for (int loopCounter = 0; loopCounter < names.size(); loopCounter++) {
            Task task = taskService().createTaskQuery().singleResult();
            Assertions.assertEquals("Say hello " + names.get(loopCounter), task.getName());
            taskService().complete(task.getId());
        }

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "EndEvent"});
    }
}
