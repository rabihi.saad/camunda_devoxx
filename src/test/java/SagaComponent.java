import com.example.workflow.delegate.ParamApiDelegate;
import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests;
import org.camunda.bpm.engine.test.junit5.ProcessEngineExtension;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

@ExtendWith(MockitoExtension.class)
@org.camunda.bpm.engine.test.Deployment(resources = {"saga.bpmn"})
public class SagaComponent {

    @RegisterExtension
    ProcessEngineExtension extension = ProcessEngineExtension.builder()
            .build();

    @Test
    public void happyPath() throws Exception {
        // Create a delegate mock and register it in camunda beans
        ParamApiDelegate delegate = Mockito.mock(ParamApiDelegate.class);
        Mockito.doNothing().when(delegate).execute(Mockito.any(DelegateExecution.class), Mockito.anyString(), Mockito.anyString());
        Mocks.register("paramApiDelegate", delegate);

        // Start saga process
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("saga");
        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        // Complete Task1
        taskService().complete(taskService().createTaskQuery().singleResult().getId());
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1"});

        // Complete Task2
        taskService().complete(taskService().createTaskQuery().singleResult().getId());
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1", "task2", "serviceTask2"});

        // Complete Task3
        taskService().complete(taskService().createTaskQuery().singleResult().getId());
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1", "task2", "serviceTask2", "task3"});

        // Task3 has an asynchronous continuation, so a job is waiting and has to be launched
        BpmnAwareTests.execute(BpmnAwareTests.job(managementService().createJobQuery().processInstanceId(processInstance.getId()), processInstance));

        // The process is complete now
        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1", "task2", "serviceTask2", "task3", "serviceTask3", "EndEvent"});
    }


    @Test
    public void errorPathAsTask3() throws Exception {
        // Create a delegate mock and register it in camunda beans
        ParamApiDelegate delegate = Mockito.mock(ParamApiDelegate.class);
        Mockito.doNothing().when(delegate).execute(Mockito.any(DelegateExecution.class), Mockito.anyString(), Mockito.anyString());
        Mocks.register("paramApiDelegate", delegate);

        // Start saga process
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("saga");
        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        // Complete Task1
        taskService().complete(taskService().createTaskQuery().singleResult().getId());
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1"});

        // Complete Task2
        taskService().complete(taskService().createTaskQuery().singleResult().getId());
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1", "task2", "serviceTask2"});

        Mockito.doThrow(new BpmnError("BUSINESS_ERROR")).when(delegate).execute(Mockito.any(DelegateExecution.class), Mockito.anyString(), Mockito.anyString());

        // Complete Task3
        taskService().complete(taskService().createTaskQuery().singleResult().getId());
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1", "task2", "serviceTask2", "task3"});

        // Task3 has an asynchronous continuation, so a job is waiting and has to be launched
        BpmnAwareTests.execute(BpmnAwareTests.job(managementService().createJobQuery().processInstanceId(processInstance.getId()), processInstance));
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1", "task2", "serviceTask2", "task3", "serviceTask3", "errorTask3", "cancelTask2", "cancelTask1"});

        // The process is complete now
        //BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] {"StartEvent", "task1", "serviceTask1", "task2", "serviceTask2", "task3", "serviceTask3", "EndEvent"});
    }
}
