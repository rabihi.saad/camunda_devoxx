import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests;
import org.camunda.bpm.engine.test.junit5.ProcessEngineExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.stream.Collectors;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

@ExtendWith(MockitoExtension.class)
@Deployment(resources = {"singleUserTask.bpmn", "twoUserTask.bpmn" , "parallelTwoUserTask.bpmn"})
public class CamundaModelExecution {


    @RegisterExtension
    ProcessEngineExtension extension = ProcessEngineExtension.builder()
            .build();


    @Test
    public void singleUserTask() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("singleUserTask");

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        List<Execution> executions = runtimeService().createExecutionQuery().list();
        Assertions.assertEquals(1, executions.size());
        Assertions.assertEquals(executions.get(0).getId(), processInstance.getId());

        Task task = taskService().createTaskQuery().list().get(0);

        taskService().complete(task.getId());

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "EndEvent"});
    }

    @Test
    public void twoUserTask() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("twoUserTask");

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        String executionId1 = runtimeService().createExecutionQuery().singleResult().getId();
        Assertions.assertEquals(executionId1, processInstance.getId());

        Task task1 = taskService().createTaskQuery().taskDefinitionKey("userTask1").singleResult();
        taskService().complete(task1.getId());

        String executionId2 = runtimeService().createExecutionQuery().singleResult().getId();

        Assertions.assertEquals(executionId2, processInstance.getId());
        Assertions.assertEquals(executionId1, executionId2);

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1"});

        Task task2 = taskService().createTaskQuery().taskDefinitionKey("userTask2").singleResult();
        taskService().complete(task2.getId());

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "userTask2", "EndEvent"});
    }

    @Test
    public void parallelTwoUserTask() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("parallelTwoUserTask");

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        List<Execution> executions = runtimeService().createExecutionQuery().list();
        Assertions.assertEquals(3, executions.size());

        executions = executions.stream().filter(ex -> !ex.getId().equals(processInstance.getId())).collect(Collectors.toList());

        Assertions.assertNotEquals(executions.get(0).getId(), processInstance.getId());
        Assertions.assertNotEquals(executions.get(1).getId(), processInstance.getId());

        Task task1 = taskService().createTaskQuery().taskDefinitionKey("userTask1").singleResult();
        taskService().complete(task1.getId());

        executions = runtimeService().createExecutionQuery().list();
        Assertions.assertEquals(3, executions.size());

        executions = executions.stream().filter(ex -> !ex.getId().equals(processInstance.getId())).collect(Collectors.toList());

        Assertions.assertNotEquals(executions.get(0).getId(), processInstance.getId());
        Assertions.assertNotEquals(executions.get(1).getId(), processInstance.getId());

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1"});

        Task task2 = taskService().createTaskQuery().taskDefinitionKey("userTask2").singleResult();
        taskService().complete(task2.getId());

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "userTask2", "EndEvent"});
    }

    @Test
    public void parallelTwoUserTaskKilledMidWay() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("parallelTwoUserTask");

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        List<Execution> executions = runtimeService().createExecutionQuery().list();
        Assertions.assertEquals(3, executions.size());

        executions = executions.stream().filter(ex -> !ex.getId().equals(processInstance.getId())).collect(Collectors.toList());

        Assertions.assertNotEquals(executions.get(0).getId(), processInstance.getId());
        Assertions.assertNotEquals(executions.get(1).getId(), processInstance.getId());

        Task task1 = taskService().createTaskQuery().taskDefinitionKey("userTask1").singleResult();
        taskService().complete(task1.getId());

        runtimeService().deleteProcessInstance(processInstance.getId(), "because we are evil");

        executions = runtimeService().createExecutionQuery().list();
        Assertions.assertEquals(0, executions.size());
    }
}
