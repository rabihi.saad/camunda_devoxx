import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.test.junit5.ProcessEngineExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.repositoryService;

@ExtendWith(MockitoExtension.class)
@org.camunda.bpm.engine.test.Deployment(resources = {"singleUserTask.bpmn"})
public class DeploymentCamundaSpringComponent {


    @RegisterExtension
    ProcessEngineExtension extension = ProcessEngineExtension.builder()
            .build();


    @Test
    @Order(1)
    public void defaultDeploymentTask() {
        List<Deployment> deployments = repositoryService().createDeploymentQuery().list();

        Assertions.assertEquals(1, deployments.size());
        Assertions.assertEquals("CamundaSpringCmponent.defaultDeploymentTask", deployments.get(0).getName());
    }

    @Test
    @Order(2)
    public void deployByCode() {
        List<ProcessDefinition> processDefinitions = repositoryService().createProcessDefinitionQuery().latestVersion().list();

        Assertions.assertEquals(1, processDefinitions.size());
        Assertions.assertEquals("singleUserTask", processDefinitions.get(0).getKey());

        repositoryService().createDeployment().addClasspathResource("twoUserTask.bpmn").deploy();

        processDefinitions = repositoryService().createProcessDefinitionQuery().latestVersion().list();

        Assertions.assertEquals(2, processDefinitions.size());

        Assertions.assertEquals("singleUserTask", processDefinitions.get(0).getKey());
        Assertions.assertEquals(1, processDefinitions.get(0).getVersion());
        Assertions.assertEquals("twoUserTask", processDefinitions.get(1).getKey());
        Assertions.assertEquals(1, processDefinitions.get(1).getVersion());

        repositoryService().createDeployment().addClasspathResource("twoUserTask.bpmn").deploy();

        processDefinitions = repositoryService().createProcessDefinitionQuery().latestVersion().list();

        Assertions.assertEquals(2, processDefinitions.size());

        Assertions.assertEquals("singleUserTask", processDefinitions.get(0).getKey());
        Assertions.assertEquals(1, processDefinitions.get(0).getVersion());
        Assertions.assertEquals("twoUserTask", processDefinitions.get(1).getKey());
        Assertions.assertEquals(2, processDefinitions.get(1).getVersion());

        repositoryService().createDeployment().addClasspathResource("singleUserTask.bpmn").addClasspathResource("twoUserTask.bpmn").deploy();

        processDefinitions = repositoryService().createProcessDefinitionQuery().latestVersion().list();

        Assertions.assertEquals(2, processDefinitions.size());

        Assertions.assertEquals("singleUserTask", processDefinitions.get(0).getKey());
        Assertions.assertEquals(2, processDefinitions.get(0).getVersion());
        Assertions.assertEquals("twoUserTask", processDefinitions.get(1).getKey());
        Assertions.assertEquals(3, processDefinitions.get(1).getVersion());
    }
}
