import com.example.workflow.delegate.Api1Delegate;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.runtime.Incident;
import org.camunda.bpm.engine.runtime.Job;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests;
import org.camunda.bpm.engine.test.junit5.ProcessEngineExtension;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

@ExtendWith(MockitoExtension.class)
@org.camunda.bpm.engine.test.Deployment(resources = {"singleUserTaskServiceTask.bpmn", "singleUserTaskAsyncServiceTask.bpmn"})
public class ServiceTasksTest {

    @RegisterExtension
    ProcessEngineExtension extension = ProcessEngineExtension.builder()
            .build();

    @Test
    public void synchTaskHappyPath() throws Exception {
        Api1Delegate delegate = Mockito.mock(Api1Delegate.class);
        Mockito.doNothing().when(delegate).execute(Mockito.any(DelegateExecution.class));
        Mocks.register("api1Delegate", delegate);

        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("singleUserTaskServiceTask");
        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        Assertions.assertEquals(1, taskService().createTaskQuery().processInstanceId(processInstance.getId()).list().size());

        taskService().complete(taskService().createTaskQuery().processInstanceId(processInstance.getId()).singleResult().getId());

        Assertions.assertEquals(0, taskService().createTaskQuery().processInstanceId(processInstance.getId()).list().size());

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "testAPI", "EndEvent"});
    }

    @Test
    public void synchTaskServiceError() throws Exception {
        Api1Delegate delegate = Mockito.mock(Api1Delegate.class);
        Mockito.doThrow(new RuntimeException()).when(delegate).execute(Mockito.any(DelegateExecution.class));
        Mocks.register("api1Delegate", delegate);

        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("singleUserTaskServiceTask");
        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        Assertions.assertEquals(1, taskService().createTaskQuery().processInstanceId(processInstance.getId()).list().size());

        Assertions.assertThrows(RuntimeException.class,
                () -> taskService().complete(taskService().createTaskQuery().processInstanceId(processInstance.getId()).singleResult().getId()));

        Assertions.assertEquals(1, taskService().createTaskQuery().processInstanceId(processInstance.getId()).list().size());

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});
    }

    @Test
    public void asynchTaskHappyPath() throws Exception {
        Api1Delegate delegate = Mockito.mock(Api1Delegate.class);
        Mockito.doNothing().when(delegate).execute(Mockito.any(DelegateExecution.class));
        Mocks.register("api1Delegate", delegate);

        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("singleUserTaskAsyncServiceTask");
        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        Assertions.assertEquals(1, taskService().createTaskQuery().processInstanceId(processInstance.getId()).list().size());

        taskService().complete(taskService().createTaskQuery().processInstanceId(processInstance.getId()).singleResult().getId());

        Assertions.assertEquals(0, taskService().createTaskQuery().processInstanceId(processInstance.getId()).list().size());

        Assertions.assertEquals(1, managementService().createJobQuery().processInstanceId(processInstance.getId()).list().size());

        BpmnAwareTests.execute(BpmnAwareTests.job(managementService().createJobQuery().processInstanceId(processInstance.getId()), processInstance));

        Assertions.assertEquals(0, managementService().createJobQuery().processInstanceId(processInstance.getId()).list().size());

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "testAPI", "EndEvent"});
    }

    @Test
    public void asynchTaskServiceError() throws Exception {
        Api1Delegate delegate = Mockito.mock(Api1Delegate.class);
        Mockito.doThrow(new RuntimeException("There was an exception")).when(delegate).execute(Mockito.any(DelegateExecution.class));
        Mocks.register("api1Delegate", delegate);

        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("singleUserTaskAsyncServiceTask");
        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        Assertions.assertEquals(1, taskService().createTaskQuery().processInstanceId(processInstance.getId()).list().size());

        taskService().complete(taskService().createTaskQuery().processInstanceId(processInstance.getId()).singleResult().getId());

        Assertions.assertEquals(0, taskService().createTaskQuery().processInstanceId(processInstance.getId()).list().size());

        Assertions.assertEquals(1, managementService().createJobQuery().processInstanceId(processInstance.getId()).list().size());

        Assertions.assertThrows(RuntimeException.class,
                () -> BpmnAwareTests.execute(BpmnAwareTests.job(managementService().createJobQuery().processInstanceId(processInstance.getId()), processInstance)));

        Assertions.assertEquals(1, managementService().createJobQuery().processInstanceId(processInstance.getId()).list().size());

        Job job = managementService().createJobQuery().processInstanceId(processInstance.getId()).singleResult();

        Assertions.assertEquals(2, job.getRetries());

        Assertions.assertThrows(RuntimeException.class,
                () -> BpmnAwareTests.execute(BpmnAwareTests.job(managementService().createJobQuery().processInstanceId(processInstance.getId()), processInstance)));

        job = managementService().createJobQuery().processInstanceId(processInstance.getId()).singleResult();

        Assertions.assertEquals(1, job.getRetries());

        Assertions.assertThrows(RuntimeException.class,
                () -> BpmnAwareTests.execute(BpmnAwareTests.job(managementService().createJobQuery().processInstanceId(processInstance.getId()), processInstance)));

        job = managementService().createJobQuery().processInstanceId(processInstance.getId()).singleResult();

        Assertions.assertEquals(0, job.getRetries());

        List<Incident> incidents = runtimeService().createIncidentQuery().processInstanceId(processInstance.getId()).list();
        Assertions.assertEquals(1, incidents.size());
        Assertions.assertEquals("There was an exception", incidents.get(0).getIncidentMessage());

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1"});
    }
}
