import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests;
import org.camunda.bpm.engine.test.junit5.ProcessEngineExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.camunda.bpm.engine.test.assertions.bpmn.BpmnAwareTests.*;

@ExtendWith(MockitoExtension.class)
@org.camunda.bpm.engine.test.Deployment(resources = {"singleUserTask.bpmn"})
public class RuntimeCamundaSpringComponent {

    @RegisterExtension
    ProcessEngineExtension extension = ProcessEngineExtension.builder()
            .build();

    @Test
    public void processInstanceVariables() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("singleUserTask");

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        runtimeService().setVariable(processInstance.getId(), "varName", "varValue");

        String value = (String) runtimeService().getVariable(processInstance.getId(), "varName");

        Assertions.assertEquals("varValue", value);

        Task task = taskService().createTaskQuery().list().get(0);

        taskService().complete(task.getId());

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "EndEvent"});
    }

    @Test
    public void processInstanceSuspend() {
        ProcessInstance processInstance = runtimeService().startProcessInstanceByKey("singleUserTask");

        BpmnAwareTests.assertThat(processInstance).isNotEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent"});

        List<ProcessInstance> list = runtimeService().createProcessInstanceQuery().processInstanceId(processInstance.getId()).list();

        Assertions.assertEquals(1, list.size());

        runtimeService().suspendProcessInstanceById(processInstance.getId());

        list = runtimeService().createProcessInstanceQuery().processInstanceId(processInstance.getId()).list();

        Assertions.assertEquals(1, list.size());

        Assertions.assertTrue(list.get(0).isSuspended());

        Task task = taskService().createTaskQuery().list().get(0);

        Assertions.assertThrows(NullPointerException.class,
                () -> taskService().complete(task.getId()));

        runtimeService().activateProcessInstanceById(processInstance.getId());

        taskService().complete(task.getId());

        BpmnAwareTests.assertThat(processInstance).isEnded();
        assertThat(processInstance).hasPassedInOrder(new String[] { "StartEvent", "userTask1", "EndEvent"});
    }

}
