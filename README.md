# camunda_devoxx

Hello this is the project with the a configuration to follow on with the presentation on the link [Camunda Developer point of view] (https://docs.google.com/presentation/d/1YnOAeAqAww9cqZ_hKEJ1h3KYlRKj0EekivCzOL9-iyw/edit#slide=id.p)

## Project prerequisit

The mocks used by the project are confiugred on NodeRed flows, which can be run with :

```
docker-compose up
```

Docker and docker-compose must be preinstalled.

Maven must be preinstalled.

## Project build

The project can be build with maven : 

```
mvn clean install
```

